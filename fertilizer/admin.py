from django.contrib import admin
from fertilizer.models import Crop, City, Nutrient, NutrientAmount, Agronomist, Appointment, Lab, Facility


# django inline admine
class FacilityTabularInline(admin.TabularInline):
    model = Facility
    extra = 0


class NutrientAmountTabularInline(admin.TabularInline):
    model = NutrientAmount
    extra = 0


class LabTabularInline(admin.TabularInline):
    model = Lab
    extra = 0


class AgronomistTabularInline(admin.TabularInline):
    model = Agronomist
    extra = 0

# Register your models here.


@admin.register(Crop)
class CropAdmin(admin.ModelAdmin):
    fields = ['name']
    save_as = True
    inlines = [NutrientAmountTabularInline]

    class Meta:
        model = Crop


@admin.register(Nutrient)
class NutrientAdmin(admin.ModelAdmin):
    fields = ['name']
    save_as = True
    inlines = [NutrientAmountTabularInline]

    class Meta:
        model = Nutrient


@admin.register(NutrientAmount)
class NutrientAmountAdmin(admin.ModelAdmin):
    fields = ['crop', 'nutrient', 'low', 'medium']
    save_as = True

    class Meta:
        model = Nutrient


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    fields = ['name']
    save_as = True
    inlines = [LabTabularInline, AgronomistTabularInline]

    class Meta:
        model = City


@admin.register(Agronomist)
class AgronomistAdmin(admin.ModelAdmin):
    fields = ['name', 'city', 'education',
              'location', ('start_time',   'end_time')]
    save_as = True

    class Meta:
        model = Agronomist


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    fields = ['agronomist', 'name', 'mobile_no', 'address', 'date', 'time']
    save_as = True

    class Meta:
        model = Appointment


@admin.register(Lab)
class LabAdmin(admin.ModelAdmin):
    fields = ['city', 'name', 'address']
    save_as = True
    inlines = [FacilityTabularInline]

    class Meta:
        model = Lab


@admin.register(Facility)
class FacilityAdmin(admin.ModelAdmin):
    fields = ['lab', 'test', 'price']
    save_as = True

    class Meta:
        model = Facility
