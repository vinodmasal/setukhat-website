from fertilizer.models import City, Crop, Agronomist, Facility, Lab, Nutrient, NutrientAmount, Appointment
from fertilizer.serializers import CropSerializer, CitySerializer, NutrientDataSerializer, AppointmentSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status

# API for a list of all crops


class CropViewSet(viewsets.ModelViewSet):
    queryset = Crop.objects.all()
    serializer_class = CropSerializer

# API for a list of all citys


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer

# API for a list of all agronomist in selected city


@api_view(['GET'])
def AgronomistList(request):
    agronomist = Agronomist.objects.all()
    city = request.query_params.get("city")
    agronomist_list = []
    if city:
        agronomist = agronomist.filter(city=city)
    for ag in agronomist:
        agronomist_list.append(
            {"city": ag.city_id, "id": ag.pk, "name": ag.name})
    return Response(agronomist_list)

# API for a details of selected agronomist


@api_view(['GET'])
def agronomists_detail(request, id):
    ag = Agronomist.objects.filter(pk=id)
    for agronomist in ag:
        ag_details = {"id": agronomist.pk, "name": agronomist.name, "education": agronomist.education,
                      "address": agronomist.location, "start_time": agronomist.start_time, "end_time": agronomist.end_time}
        return Response(ag_details)

# API for a list of all labs in selected city


@api_view(['GET'])
def lab_list(request):
    lab_name = Lab.objects.all()
    city = request.query_params.get("city")
    lab_list = []
    if city:
        lab_name = lab_name.filter(city=city)
    for l in lab_name:
        lab_list.append(
            {"city": l.city_id, "id": l.pk, "name": l.name})
    return Response(lab_list)

# API for a details of selected lab


@api_view(['GET'])
def lab_detail(request, id):
    labs = Lab.objects.filter(pk=id)
    for lab in labs:
        details = {"id": lab.pk, "name": lab.name,
                   "address": lab.address}
        facility = Facility.objects.filter(lab=lab.pk)
        for fac in facility:
            details[fac.test] = fac.price
        return Response(details)

# API for a evaluate nutrient amount


@api_view(['POST'])
def evaluate(request):
    sz = NutrientDataSerializer(data=request.data, many=False)
    sz.is_valid(raise_exception=True)
    data = sz.validated_data
    crop = Crop.objects.get(id=data['crop_id'])
    # store input data
    nitrogen = data['nitrogen']
    phosphorus = data['phosphorus']
    potassium = data['potassium']

# comparing nutrient amount to user_input data for nitrogen
    NA = NutrientAmount.objects.get(
        crop=crop, nutrient=Nutrient.objects.get(name="Nitrogen"))
    if nitrogen < NA.low:
        nitrogen = "low"
    elif nitrogen < NA.medium:
        nitrogen = "medium"
    else:
        nitrogen = "high"

# comparing nutrient amount to user_input data for phosphorus
    NA = NutrientAmount.objects.get(
        crop=crop, nutrient=Nutrient.objects.get(name="Phosphorus"))
    if phosphorus < NA.low:
        phosphorus = "low"
    elif phosphorus < NA.medium:
        phosphorus = "medium"
    else:
        phosphorus = "high"

# comparing nutrient amount to user_input data for potassium
    NA = NutrientAmount.objects.get(
        crop=crop, nutrient=Nutrient.objects.get(name="Potassium"))
    if potassium < NA.low:
        potassium = "low"
    elif potassium < NA.medium:
        potassium = "medium"
    else:
        potassium = "high"

    # evaluate data

    if nitrogen == "medium" and phosphorus == "medium" and potassium == "medium":
        to_show = False
    else:
        to_show = True

    # final output to the client
    result = {"crop_id": crop.pk, "nitrogen": nitrogen,
              "phosphorus": phosphorus, "potassium": potassium, "to_show": to_show}

    return Response(result, status=status.HTTP_200_OK)

# API for a store appointment details


@api_view(['POST'])
def appointment(request):
    sz = AppointmentSerializer(data=request.data, many=False)
    sz.is_valid(raise_exception=True)
    data = sz.validated_data
    agronomist = data['agronomist']
    Appointment.objects.create(
        agronomist=agronomist, name=data['name'], mobile_no=data['mobile_no'], address=data['address'], date=data['date'], time=data['time'])

    return Response("Appointment Booked Successfully", status=status.HTTP_201_CREATED)
