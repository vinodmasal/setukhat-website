
from django.db import models
from phone_field import PhoneField
from django.core.validators import MinLengthValidator, MaxLengthValidator

# Create your models here.

#  crop model


class Crop(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return (self.name)

#  Nutrient model


class Nutrient(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return (self.name)

#  Nutrient amount model


class NutrientAmount(models.Model):
    crop = models.ForeignKey(Crop, on_delete=models.CASCADE)
    nutrient = models.ForeignKey(Nutrient, on_delete=models.CASCADE)
    low = models.FloatField(default=0, help_text='Please enter data in kg/ha')
    medium = models.FloatField(
        default=0, help_text='Please enter data in kg/ha')

    class Meta:
        unique_together = ['crop', 'nutrient']

    def __str__(self):
        return (f"{self.crop}-{self.nutrient}")

#  city model


class City(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return (self.name)

#  Agronomist model


class Agronomist(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    education = models.CharField(max_length=100)
    location = models.TextField()
    start_time = models.TimeField()
    end_time = models.TimeField()

    class Meta:
        unique_together = ['name', 'city', 'location']

    def __str__(self):
        return (f'{self.name}-{self.city}')

#  Appointment model


class Appointment(models.Model):
    agronomist = models.ForeignKey(Agronomist, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    mobile_no = PhoneField(validators=[MinLengthValidator(
        10), MaxLengthValidator(13)], blank=True)
    address = models.TextField()
    date = models.DateField(null=True)
    time = models.TimeField(null=True)

    class Meta:
        # unique_together = ['name', 'slot']
        unique_together = ['name', 'date', 'time']

    def __str__(self):
        # return (f'{self.name}-{self.slot}')
        return (f'{self.name}')

#  Lab model


class Lab(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    address = models.TextField()

    class Meta:
        unique_together = ['city', 'name', 'address']

    def __str__(self):
        return (f'{self.name}-{self.city}')

#  Facility model


class Facility(models.Model):
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
    test = models.CharField(max_length=200)
    price = models.FloatField(default=0, help_text='Rs-INR')

    class Meta:
        unique_together = ['lab', 'test', 'price']

    def __str__(self):
        return (f'{self.lab}-{self.test}-{self.price}')
