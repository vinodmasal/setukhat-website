from django.urls import path
from rest_framework import routers
from fertilizer.views import CropViewSet, CityViewSet, AgronomistList,  agronomists_detail, lab_list, lab_detail, evaluate, appointment
router = routers.DefaultRouter()


router.register(r'crop', CropViewSet)
router.register(r'city', CityViewSet)
urlpatterns = router.urls

urlpatterns += [
    path('agronomists/', AgronomistList),
    path('labs/', lab_list),
    path('agronomist/<id>', agronomists_detail),
    path('lab/<id>', lab_detail),
    path('evaluate/', evaluate),
    path('appointment/', appointment),
]
