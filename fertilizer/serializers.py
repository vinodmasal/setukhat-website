from rest_framework import serializers
from fertilizer.models import Appointment, City, Crop


# Crop Serializer
class CropSerializer(serializers.ModelSerializer):
    class Meta:
        model = Crop
        fields = ['id', 'name']

# City Serializer


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ['id', 'name']

# NutrientData evaluation Serializer


class NutrientDataSerializer(serializers.Serializer):
    crop_id = serializers.IntegerField()
    nitrogen = serializers.FloatField()
    phosphorus = serializers.FloatField()
    potassium = serializers.FloatField()

# Appointment serializer


class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ['agronomist', 'name', 'mobile_no', 'address', 'date', 'time']
